-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-05-2020 a las 06:02:25
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_cms`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

CREATE TABLE `comentarios` (
  `id_comentario` bigint(3) UNSIGNED NOT NULL,
  `nom_cliente` varchar(64) NOT NULL,
  `email_cliente` varchar(128) NOT NULL,
  `asunto_comentario` varchar(64) DEFAULT NULL,
  `detalle` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imgs_galeria`
--

CREATE TABLE `imgs_galeria` (
  `id_img` bigint(3) UNSIGNED NOT NULL,
  `nombre_img` varchar(64) NOT NULL,
  `descripcion_img` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `secciones`
--

CREATE TABLE `secciones` (
  `id_seccion` bigint(3) UNSIGNED NOT NULL,
  `id_usuario` bigint(3) UNSIGNED NOT NULL,
  `titulo_seccion` varchar(128) NOT NULL,
  `banner_seccion` varchar(64) DEFAULT NULL,
  `detalle_seccion` varchar(2500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sec_contacto`
--

CREATE TABLE `sec_contacto` (
  `id` bigint(3) UNSIGNED NOT NULL,
  `banner` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sec_contacto`
--

INSERT INTO `sec_contacto` (`id`, `banner`) VALUES
(1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sec_galeria`
--

CREATE TABLE `sec_galeria` (
  `id` bigint(3) UNSIGNED NOT NULL,
  `banner` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sec_galeria`
--

INSERT INTO `sec_galeria` (`id`, `banner`) VALUES
(1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sec_inicio_quienessomos`
--

CREATE TABLE `sec_inicio_quienessomos` (
  `id` bigint(3) UNSIGNED NOT NULL,
  `tipo` varchar(1) NOT NULL,
  `detalle` varchar(2500) NOT NULL,
  `banner` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sec_inicio_quienessomos`
--

INSERT INTO `sec_inicio_quienessomos` (`id`, `tipo`, `detalle`, `banner`) VALUES
(1, 'I', '', NULL),
(2, 'Q', '', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sec_servicios`
--

CREATE TABLE `sec_servicios` (
  `id` bigint(3) UNSIGNED NOT NULL,
  `banner` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sec_servicios`
--

INSERT INTO `sec_servicios` (`id`, `banner`) VALUES
(1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `id_servicio` bigint(3) UNSIGNED NOT NULL,
  `nom_servicio` varchar(128) NOT NULL,
  `detalle_servicio` varchar(1200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` bigint(3) UNSIGNED NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(128) NOT NULL,
  `nom_real` varchar(64) NOT NULL,
  `foto` varchar(32) DEFAULT NULL,
  `email` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `username`, `password`, `nom_real`, `foto`, `email`) VALUES
(1, 'admin', '$2y$10$oYBpQ4RvkkpAFZ5C.zporuFC4mZb7bnfDmEKOtb03Ks550SkabvtS', 'administrador', NULL, 'admin@administrador.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`id_comentario`),
  ADD UNIQUE KEY `id_comentario` (`id_comentario`);

--
-- Indices de la tabla `imgs_galeria`
--
ALTER TABLE `imgs_galeria`
  ADD PRIMARY KEY (`id_img`),
  ADD UNIQUE KEY `id_img` (`id_img`);

--
-- Indices de la tabla `secciones`
--
ALTER TABLE `secciones`
  ADD PRIMARY KEY (`id_seccion`),
  ADD UNIQUE KEY `id_seccion` (`id_seccion`),
  ADD KEY `secciones_fk1` (`id_usuario`);

--
-- Indices de la tabla `sec_contacto`
--
ALTER TABLE `sec_contacto`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `sec_galeria`
--
ALTER TABLE `sec_galeria`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `sec_inicio_quienessomos`
--
ALTER TABLE `sec_inicio_quienessomos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `sec_servicios`
--
ALTER TABLE `sec_servicios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`id_servicio`),
  ADD UNIQUE KEY `id_servicio` (`id_servicio`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `id_usuario` (`id_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `id_comentario` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `imgs_galeria`
--
ALTER TABLE `imgs_galeria`
  MODIFY `id_img` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `secciones`
--
ALTER TABLE `secciones`
  MODIFY `id_seccion` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sec_contacto`
--
ALTER TABLE `sec_contacto`
  MODIFY `id` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sec_galeria`
--
ALTER TABLE `sec_galeria`
  MODIFY `id` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sec_inicio_quienessomos`
--
ALTER TABLE `sec_inicio_quienessomos`
  MODIFY `id` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sec_servicios`
--
ALTER TABLE `sec_servicios`
  MODIFY `id` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `servicios`
--
ALTER TABLE `servicios`
  MODIFY `id_servicio` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` bigint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `secciones`
--
ALTER TABLE `secciones`
  ADD CONSTRAINT `secciones_fk1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
