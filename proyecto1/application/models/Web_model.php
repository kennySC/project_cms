<?php

class Web_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    public function add_comentario($params)
    {
        $this->db->insert('comentarios', $params);
        return $this->db->insert_id();
    }
    function get_sec_banner($tabla)
    {
        $banner = $this->db->query("SELECT banner FROM " . $tabla)->row_array();
        if ($banner == null) {
            $banner['banner'] = 'n';
        }
        return $banner;
    }

    function get_banner_id($tabla, $id){
        if($tabla == "secciones"){
            $banner = $this->db->query("SELECT banner_seccion FROM ".$tabla." where id_seccion = '". $id ."'")->row_array();
        }else{
            $banner = $this->db->query("SELECT banner FROM ".$tabla." where id = '". $id ."'")->row_array();
        }
        if ($banner == null) {
            $banner['banner'] = 'sin_banner';
        }
        return $banner;
    }

    function get_inicioQS_info($tipo)
    {

        $info = $this->db->query("SELECT tipo, detalle, banner FROM  sec_inicio_quienessomos WHERE tipo ='" . $tipo . "'")->row_array();
        if ($info == null) {
            $info['banner'] = 'n';
            $info['detalle'] = '';
        }
        if ($tipo == 'I') {
            $info['titulo'] = 'Inicio';
        } else {
            $info['titulo'] = '¿Quienes Somos?';
        }
        return $info;
    }

    function get_seccion_menu()
    {
        return $this->db->query("SELECT id_seccion,titulo_seccion FROM  secciones")->result_array();
    }
    function get_img_galeria()
    {
        $this->db->limit(10);
        $query = $this->db->get('imgs_galeria');
        return $query->result_array();
    }
    function get_servicios()
    {
        return $this->db->get('servicios')->result_array();
    }
    function get_seccion_info($id)
    {
        return $this->db->query("SELECT id_seccion,titulo_seccion,banner_seccion,detalle_seccion FROM  secciones WHERE id_seccion=" . $id)->row_array();
    }

    function actualizar_banner($tabla, $id, $params)
    {
        if($tabla == "secciones"){
            $this->db->where('id_seccion', $id);        
        }else{
            $this->db->where('id', $id);        
        }        
        return $this->db->update($tabla, $params);
    }

    function find_img($id_img){
        return $this->db->query("SELECT nombre_img FROM imgs_galeria where id_img = '". $id_img ."'")->row_array();        
    }

    function add_img_galeria($params){
        $this->db->insert('imgs_galeria', $params);
        return $this->db->insert_id();
    }

    function del_img_galeria($id_img){
        return $this->db->delete('imgs_galeria',array('id_img'=>$id_img));
    }
}
