<?php
class Seccion_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    function get_secciones(){
        return $this->db->query("SELECT sec_inicio_quienessomos.id, sec_inicio_quienessomos.tipo, sec_inicio_quienessomos.detalle, secciones.id_seccion, secciones.titulo_seccion, secciones.banner_seccion, secciones.detalle_seccion
                                 FROM sec_inicio_quienessomos, secciones")->result_array();
    }

    function get_secciones_agregadas(){
        return $this->db->query("SELECT * FROM secciones")->result_array();
    }

    function get_ini_quienes() {
        return $this->db->query("SELECT * FROM sec_inicio_quienessomos")->result_array();
    }

    function get_data_ini_id($id) {
        return $this->db->query("SELECT * FROM sec_inicio_quienessomos WHERE id = ". $id)->row_array();
    }

    function get_data_seccion($id_seccion){
        return $this->db->query("SELECT secciones.id_seccion, secciones.id_usuario, secciones.titulo_seccion, secciones.banner_seccion, secciones.detalle_seccion
                                    FROM secciones
                                    WHERE secciones.id_seccion = " . $id_seccion)->row_array();
    }

    function agregar_seccion($params){
        $this->db->insert('secciones', $params);
        return $this->db->insert_id();
    }

    function editar_seccion($tabla, $id, $params){
        if($tabla == "secciones"){
            $this->db->where('id_seccion', $id);
        }else {
            $this->db->where('id', $id);
        }
        echo $tabla;
        return $this->db->update($tabla, $params);
    }

    function eliminar_seccion($id_seccion){
        return $this->db->delete('secciones',array('id_seccion'=>$id_seccion));
    }

    function agregar_servicio($params){
        $this->db->insert('servicios', $params);
        return $this->db->insert_id();
    }

    function editar_servicio($params){
        return $this->db->query("UPDATE servicios SET servicios.nom_servicio = '" . $params['nom_servicio'] . "' , servicios.detalle_servicio = '" . $params['detalle_servicio'] . "' 
                                WHERE servicios.id_servicio = " . $params['id_servicio']);
    }

    function eliminar_servicio($id){
        $this->db->delete('servicios', array('id_servicio' => $id));
    }

    function get_data_servicio($id){
        return $this->db->query("SELECT servicios.id_servicio, servicios.nom_servicio, servicios.detalle_servicio
                                FROM servicios
                                WHERE servicios.id_servicio = $id")->row_array();
    }
}    