<?php

	Class Auth_model extends CI_Model {
        
        public function login($data){
            $usuario_existe = $this->validar_usuario($data['username']);

            if($usuario_existe != false && password_verify($data['password'], $usuario_existe[0]->password)){
                return true; //El usuario existe, y fue autenticado
            }
            else{
                return false; //NO autenticado
            }
        }

        public function validar_usuario($username){
            $query = $this->db->query("SELECT usuarios.* FROM usuarios WHERE usuarios.username = '$username'");
            if ($query->num_rows() == 1) {
                return $query->result();
            } else {
                return false;
            }
        }

    }