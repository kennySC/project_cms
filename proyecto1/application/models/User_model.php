<?php
class User_model extends CI_Model{
    
    function __construct(){
        parent::__construct();
    }

    function obtener_usuario($users_id){
        return $this->db->query("SELECT usuarios.* FROM usuarios WHERE usuarios.id_usuario = " . $users_id)->row_array();
    }

    function agregar_usuario($params){
        $this->db->insert('usuarios', $params);
        return $this->db->insert_id();
    }

    function actualizar_usuario($id_usuario, $params){
        $this->db->where('id_usuario', $id_usuario);
        return $this->db->update('usuarios', $params);
    }

    function eliminar_usuario($users_id){
        return $this->db->delete('usuarios',array('id_usuario'=>$users_id));
    }
}    