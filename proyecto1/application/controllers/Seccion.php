<?php

    class Seccion extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Web_model');
        $this->load->model('Seccion_model');
        $this->load->database();
    } 

    function volver($vistaactual){
        $data['_view'] = 'secciones/'.$vistaactual;
        $this->load->view('layouts/main',$data);
    }

    function ir_editar($tabla, $id){        
        if($tabla == "sec_inicio_quienessomos"){
            $data['seccion'] = $this->Seccion_model->get_data_ini_id($id);            
            $data['tipo'] = "ini";
        }else {
            $data['seccion'] = $this->Seccion_model->get_data_seccion($id);
            $data['tipo'] = "sec";
        }
        $this->load->view('secciones/editar_seccion',$data);
    }

    function agregar(){
        $this->load->library('form_validation');

        $this->form_validation->set_rules('txt_titulo','Titulo','required|max_length[128]');
        $this->form_validation->set_rules('txt_detalle','Detalle','required|max_length[2500]');

        if($this->form_validation->run()){

            $config['upload_path']          = './resources/files/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 5000; //5MB
            $config['overwrite']            = true;

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('input_banner')) {
                $params = array(
                    'id_usuario' => $this->session->userdata['logged_in']['id_usuario'],
                    'titulo_seccion' => $this->input->post('txt_titulo'),
                    'banner_seccion' => $this->upload->data('file_name'),
                    'detalle_seccion' => $this->input->post('txt_detalle')
                );
            }
            else{
                $params = array(
                    'id_usuario' => $this->session->userdata['logged_in']['id_usuario'],
                    'titulo_seccion' => $this->input->post('txt_titulo'),
                    'banner_seccion' => 'no_banner',
                    'detalle_seccion' => $this->input->post('txt_detalle')
                );
            }
            $this->Seccion_model->agregar_seccion($params);
            $data['_view'] = 'admin/index';
        }
        else{
            $data['_view'] = 'secciones/agregar_seccion';
        }
        $this->load->view('layouts/main', $data);
    }

    function editar($tabla, $id){
        $this->load->library('form_validation');
        
        if($tabla == "secciones"){
            $this->form_validation->set_rules('txt_titulo','Titulo','required|max_length[128]');        
        }
        $this->form_validation->set_rules('txt_detalle','Detalle','required|max_length[2500]');

        if($this->form_validation->run()){
            if($tabla == "secciones"){
                $params = array(
                    'id_usuario' => $this->session->userdata['logged_in']['id_usuario'],
                    'titulo_seccion' => $this->input->post('txt_titulo'),
                    'detalle_seccion' => $this->input->post('txt_detalle')
                );
            }else{
                $params = array(                                    
                    'detalle' => $this->input->post('txt_detalle')
                );
            }
            $this->Seccion_model->editar_seccion($tabla, $id, $params);            
            $this->session->set_flashdata('succes', "Todo bien, todo correcto.");
        }
        else{            
            $this->session->set_flashdata('error', "fallo la validacion.");
        }
        $this->ir_editar($tabla, $id);

    }

    function eliminar($id_seccion){
        
        $data['seccion'] = $this->Seccion_model->get_data_seccion($id_seccion);
        
        $path = "./resources/files/".$data["banner"];
        unlink($path);

        if($this->session->userdata['logged_in']['id_usuario'] == $data['seccion']['id_usuario']){
            $this->Seccion_model->eliminar_seccion($id_seccion);
        }

        redirect("admin/lista_secciones");
    }

    function lista_servicios(){
        $data['banner'] = $this->Web_model->get_sec_banner('sec_servicios');
        $data['servicios'] = $this->Web_model->get_servicios();
        $data['_view'] = 'secciones/lista_servicios';
        $this->load->view('layouts/main', $data);
    }

    function editar_galeria(){
        $data['banner_gal'] = $this->Web_model->get_sec_banner("sec_galeria");
        $data['imgs'] = $this->Web_model->get_img_galeria();
        $data['_view'] = 'secciones/editar_galeria';
        $this->load->view('layouts/main', $data);
    }

    function agregar_servicio(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txt_nombre_servicio', 'Servicio', 'required|max_length[128]');
        $this->form_validation->set_rules('txt_detalle_servicio', 'Detalle Servicio', 'required|max_length[1200]');

        if($this->form_validation->run()){
            $params = array(
                'nom_servicio' => $this->input->post('txt_nombre_servicio'),
                'detalle_servicio' => $this->input->post('txt_detalle_servicio')
            );
            $this->Seccion_model->agregar_servicio($params);
        }
        $this->lista_servicios();
    }

    function editar_servicio($id_servicio){
        $data['servicio'] = $this->Seccion_model->get_data_servicio($id_servicio);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txt_nombre_servicio', 'Servicio', 'required|max_length[128]');
        $this->form_validation->set_rules('txt_detalle_servicio', 'Detalle Servicio', 'required|max_length[1200]');

        if($this->form_validation->run()){
            $params = array(
                'id_servicio' => $id_servicio,
                'nom_servicio' => $this->input->post('txt_nombre_servicio'),
                'detalle_servicio' => $this->input->post('txt_detalle_servicio')
            );
            $this->Seccion_model->editar_servicio($params);
            $this->lista_servicios();
        }
        $data['_view'] = 'secciones/editar_servicio';
        $this->load->view('layouts/main', $data);
    }

    function eliminar_servicio($id_servicio){
        $this->Seccion_model->eliminar_servicio($id_servicio);
        $this->lista_servicios();
    }

    function subir_banner($tabla, $id, $vistaactual){

        $config['upload_path']          = './resources/files/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 5000; //5MB
        $config['file_name']           = $tabla . $id;
        $config['overwrite']            = true;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('txt_banner'))
        {
            $error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('error', $error['error']);
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            if($tabla == "secciones"){
                $params = array(
                    'banner_seccion' => $this->upload->data('file_name'),
                );
            }else{
                $params = array(
                    'banner' => $this->upload->data('file_name'),
                );
            }
            $this->Web_model->actualizar_banner($tabla, $id, $params);       
            $this->session->set_flashdata('success', "Archivo cargado al sistema exitosamente.");
        }

        if($vistaactual == "editar_galeria"){
            redirect('seccion/editar_galeria');
        }else if($vistaactual == "editar_servicios"){            
            redirect('seccion/editar_servicios');
        }else if($vistaactual == "editar_seccion"){            
            $this->ir_editar($tabla, $id);
        }else if($vistaactual == "lista_servicios"){            
            redirect('seccion/lista_servicios');
        }
    }

    function eliminar_banner($tabla, $id, $vistaactual){
        $banner = $this->Web_model->get_banner_id($tabla, $id);
        if($tabla == "secciones"){
            $params = array(
                'banner_seccion' => "sin_banner",
            );
            if($banner["banner_seccion"] != "sin_banner"){
                $path = "./resources/files/".$banner["banner"];
                unlink($path);
            }
        }else{    
            $params = array(
                'banner' => "sin_banner",
            );
            if($banner["banner"] != "sin_banner"){
                $path = "./resources/files/".$banner["banner"];
                unlink($path);
            }
        }
        $this->Web_model->actualizar_banner($tabla, $id, $params);
        $this->session->set_flashdata('success', "Archivo eliminado del sistema exitosamente.");
        if($vistaactual == "editar_galeria"){
            redirect('seccion/editar_galeria');
        }else if($vistaactual == "editar_servicios"){            
            redirect('seccion/editar_servicios');
        }else if($vistaactual == "editar_seccion"){            
            $this->ir_editar($tabla, $id);
        }else if($vistaactual == "lista_servicios"){            
            redirect('seccion/lista_servicios');
        }
    }


    function add_img_galeria(){
        $this->load->library('form_validation');
        $imgs = $this->Web_model->get_img_galeria();
        $this->form_validation->set_rules('txt_detalle_gal','Detalle','required|max_length[1000]');
        if(count($imgs) < 10){
            if($this->form_validation->run()){

                $config['upload_path']          = './resources/files/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 5000; //5MB
                $config['overwrite']            = true;

                $this->load->library('upload', $config);

                if ($this->upload->do_upload('txt_img')) {
                    $params = array(
                        'nombre_img' =>$this->upload->data('file_name'),
                        'descripcion_img' => $this->input->post('txt_detalle_gal')
                    );
                    $this->Web_model->add_img_galeria($params);
                    $this->session->set_flashdata('success', "Archivo cargado al sistema exitosamente.");
                }
                else{
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('error', $error['error']);
                }            
            }else{
                $this->session->set_flashdata('error', "Debes añadir una imagen.");
            }
        }else{
            $this->session->set_flashdata('error', "No puedes añadir mas imagenes ya que fue alcanzado el maximo de 10.");                
        }   
        redirect('seccion/editar_galeria');
    }

    function del_img_galeria($id){
        $img = $this->Web_model->find_img($id);
        $path = "./resources/files/".$img["nombre_img"];
        unlink($path);
        $this->Web_model->del_img_galeria($id);
        $this->session->set_flashdata('success', "Archivo eliminado del sistema exitosamente.");
        redirect('seccion/editar_galeria');
    }
}