<?php

Class Auth extends CI_Controller {

	public function __construct() {
		parent::__construct();

        $this->load->helper('form');
        $this->load->helper('url');
		$this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('Auth_model');
        $this->load->database();
    }
    
    /* Metodo para que de la pagina web, se pase a hacer el login para editar la pagina o agregar/editar usuarios*/
    function index(){
        $this->load->view('auth/login');
    }

    function load_data_view($view)
    {    	
        $data['_view'] = $view;
		$this->load->view('layouts/main',$data);
    }

    function login(){

        $this->form_validation->set_rules('txt_username', 'Username', 'trim|required');
		$this->form_validation->set_rules('txt_password', 'Password', 'trim|required');

        if($this->form_validation->run() == FALSE){            
            if(isset($this->session->userdata['logged_in'])){                
                $this->load_data_view('admin/index');
            }
            else{
				$this->load->view('auth/login');    
			}
        }
        else{
            $data = array(
                'username' => $this->input->post('txt_username'),
                'password' => $this->input->post('txt_password')
            );

            $result = $this->Auth_model->login($data);

            if($result == TRUE){

                $username = $this->input->post('txt_username');
                $result = $this->Auth_model->validar_usuario($username);    

                if ($result != false){
                    
                    $session_data = array(
						'logged_in' => TRUE,
						'id_usuario' => $result[0]->id_usuario,
						'username' => $result[0]->username, 
						'nom_real' => $result[0]->nom_real,
                        'foto' => $result[0]->foto,
                        'email'=> $result[0]->email,
                    );
                    
                    $this->session->set_userdata('logged_in', $session_data);
                    redirect('admin/index', 'refresh');
					$this->load_data_view('admin/index'); 
                }
            }
            else{
                $data = array(
					'error_message' => 'Usuario o Contraseña incorrectos'
                );
                $this->load->view('auth/login', $data);
            }

        }
    }

    function logout(){
        // Removemos los datos de la sesion
		$sess_array = array(
			'logged_in' => FALSE,
			'username' => '',
		);
		$this->session->unset_userdata('logged_in', $sess_array);
		$this->session->sess_destroy();
		$data['message_display'] = 'Has cerrado tu sesión de forma exitosa.';
		$this->load->view('auth/login', $data);
	}

}    