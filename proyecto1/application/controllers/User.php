<?php

class User extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('User_model');
        $this->load->database();
    } 

    function index()
    {
        $data['_view'] = 'user/edit';        
        $this->load->view('layouts/main',$data);
    }

    function ir_edit(){
        $data['_view'] = 'user/edit';        
        $this->load->view('layouts/main',$data);
    }

    function process(){

        if ($this->input->post('btn_agregar_usuario')) {
            $this->agregar();
        }

        if ($this->input->post('btn_editar_usuario')) {
            $this->editar();
        }
    }

    function agregar(){

        $this->load->library('form_validation');

        $this->form_validation->set_rules('txt_username','Usuario','required|max_length[32]');
        $this->form_validation->set_rules('txt_password','Contraseña','required|max_length[128]');
        $this->form_validation->set_rules('txt_nombrereal','Nombre','required|max_length[64]');
        $this->form_validation->set_rules('txt_correo','Correo','required|max_length[64]');

        if($this->form_validation->run()){
            $params = array(
                'username' => $this->input->post('txt_username'),
                'password' => password_hash($this->input->post('txt_password'), PASSWORD_BCRYPT),
                'nom_real' => $this->input->post('txt_nombrereal'),
                'foto' => 'sin_foto',
                'email' => $this->input->post('txt_correo')
            );
            $this->User_model->agregar_usuario($params);
            $data['_view'] = 'admin/index';
        }
        else{
            $data['_view'] = 'user/add';
        }
        $this->load->view('layouts/main', $data);
    }

    function editar(){
        
        $id_usuario = $this->session->userdata['logged_in']['id_usuario'];
        $this->load->library('form_validation');

        $this->form_validation->set_rules('txt_username','Usuario','required|max_length[32]');
        $this->form_validation->set_rules('txt_password','Contraseña','required|max_length[128]');
        $this->form_validation->set_rules('txt_nombrereal','Nombre','required|max_length[64]');
        $this->form_validation->set_rules('txt_correo','Correo','required|max_length[64]');

        if($this->form_validation->run()){
            $params = array(
                'username' => $this->input->post('txt_username'),
                'password' => password_hash($this->input->post('txt_password'), PASSWORD_BCRYPT),
                'nom_real' => $this->input->post('txt_nombrereal'),
                'email' => $this->input->post('txt_correo')
            );
            
            $this->User_model->actualizar_usuario($id_usuario, $params);
            $this->session->set_flashdata('success', "Tus datos de usuario se han actualizado. Vuelve a autenticarte para ver los cambios.");
            $data['_view'] = 'admin/index';
        }
        else{
            $data['_view'] = 'user/edit';
        }
        $this->load->view('layouts/main', $data);
    }

    function eliminar($usuario){
        $this->User_model->eliminar_usuario($usuario);   
        $data['message_display'] = 'Tu cuenta se ha eliminado exitosamente. ¡Vuelve pronto!';
        $this->load->view('auth/login', $data);
    }

    function upload_photo($users_id)
    {
        $config['upload_path']          = './resources/photos/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 2000; //2MB
        $config['file_name']           = $users_id;
        $config['overwrite']            = true;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('txt_file'))
        {
            $error = array('error' => $this->upload->display_errors());
            $this->session->set_flashdata('error', $error['error']);
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $params = array(
                'foto' => $this->upload->data('file_name'),
            );

            $this->User_model->actualizar_usuario($users_id,$params);
            $this->session->userdata['logged_in']['foto'] = $this->upload->data('file_name');            
            $this->session->set_flashdata('success', "Archivo cargado al sistema exitosamente.");
        }

        redirect('user/ir_edit');
    }

    function eliminar_foto($users_id){
        $userdata = $this->User_model->obtener_usuario($users_id);
        $params = array(
            'foto' => "sin_foto",
        );
        if($userdata['foto'] != "sin_foto"){
            $path = "./resources/photos/".$userdata["foto"];
            unlink($path);
        }
        $this->User_model->actualizar_usuario($users_id, $params);
        $this->session->userdata['logged_in']['foto'] = "sin_foto";
        $this->session->set_flashdata('success', "Archivo eliminado del sistema exitosamente.");
        redirect('user/ir_edit');
    }

}    