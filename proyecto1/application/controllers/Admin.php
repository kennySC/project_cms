<?php
class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Web_model');
        $this->load->model('Seccion_model');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->database();
    }

    function index()
    {
        if(isset($this->session->userdata['logged_in'])){ 
            $data['_view'] = 'admin/index';    
        } else {
            $data['_view'] = 'auth/login';
        }     
        $this->load->view('layouts/main', $data);
    }
    
    function process(){        
        if ($this->input->post('btn_agregar_seccion')) {
            $this->agregar_seccion();
        }
        if ($this->input->post('btn_editar_seccion')) {
            $this->lista_secciones();
        }
    }

    function agregar_seccion(){
        $data['_view'] = 'secciones/agregar_seccion';
        $this->load->view('layouts/main', $data);
    }
    
    function lista_secciones(){
        $data['_view'] = 'secciones/lista_secciones';
        $data['agreagadas'] = $this->Seccion_model->get_secciones_agregadas();
        $data['ini_quienes'] = $this->Seccion_model->get_ini_quienes();
        $this->load->view('layouts/main', $data);
    }

}
