<?php
    class Web extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
            $this->load->model('Web_model');
            $this->load->helper('form');
            $this->load->helper('url');
            $this->load->library('form_validation');
            $this->load->library('session');
            $this->load->database();
        }
        function index()
        {
            $data['info'] = $this ->Web_model->get_inicioQS_info('I');
            $data['menu'] = $this->Web_model->get_seccion_menu();
            $data['_view'] = 'web/index';
            $this->load->view('layouts/main', $data);
        }
        function quienesSomos()
        {
            $data['info'] = $this ->Web_model->get_inicioQS_info('Q');
            $data['menu'] = $this->Web_model->get_seccion_menu();
            $data['_view'] = 'web/index';
            $this->load->view('layouts/main', $data);
        }
        function galeria()
        {
            $data['imgs'] = $this->Web_model->get_img_galeria();
            $data['banner'] = $this ->Web_model->get_sec_banner('sec_galeria');
            $data['menu'] = $this->Web_model->get_seccion_menu();
            $data['_view'] = 'web/galeria';
            $this->load->view('layouts/main', $data);
        }
        function contacto()
        {   $data['banner'] = $this ->Web_model->get_sec_banner('sec_contacto');
            $data['menu'] = $this->Web_model->get_seccion_menu();
            $data['_view'] = 'web/contacto';
            $this->load->view('layouts/main', $data);
        }
        function servicios()
        {
            $data['servicios'] = $this->Web_model->get_servicios();
            $data['banner'] = $this ->Web_model->get_sec_banner('sec_servicios');
            $data['menu'] = $this->Web_model->get_seccion_menu();
            $data['_view'] = 'web/servicios';
            $this->load->view('layouts/main', $data);
        }
        function seccion($id){
            $data['info'] = $this ->Web_model->get_seccion_info($id);
            $data['menu'] = $this->Web_model->get_seccion_menu();
            $data['_view'] = 'web/seccion';
            $this->load->view('layouts/main', $data);
        }
        function addCom(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('txt_nombre','nombre_comentario','required|max_length[64]');
            $this->form_validation->set_rules('txt_email','email_comentario','required|max_length[128]');
            $this->form_validation->set_rules('txt_asunto','asunto_comentario','required|max_length[64]');
            $this->form_validation->set_rules('txt_comentario','detalle_comentario','required|max_length[1000]');
            if($this->form_validation->run())
            {
                $params = array(
                'nom_cliente' => $this->input->post('txt_nombre'),
                'email_cliente' => $this->input->post('txt_email'),
                'asunto_comentario' => $this->input->post('txt_asunto'),
                'detalle' => $this->input->post('txt_comentario'),
                );
                $this->Web_model->add_comentario($params);
            }
          $this->contacto();

        }
        function TemaOscuro($vista){
            $cookie_name = 'tema';
            if(!isset($_COOKIE[$cookie_name])) {
                setcookie($cookie_name, true, 0, "/");
            } else {
                setcookie($cookie_name, "", time() - 3600,"/");
            }
            switch ($vista) {
                case 'galeria':
                    redirect('web/galeria', 'refresh');
					$this->load_data_view('web/galeria'); 
                    break;
                case 'contacto':
                    redirect('web/contacto', 'refresh');
                    $this->load_data_view('web/contacto'); 
                    break;
                case 'servicio':
                    redirect('web/servicios', 'refresh');
                    $this->load_data_view('web/servicios'); 
                    break;
                case 'Q':
                    redirect('web/quienesSomos', 'refresh');
                    $this->load_data_view('web/index'); 
                    break;
                case 'I':
                    redirect('web/index', 'refresh');
                    $this->load_data_view('web/index'); 
                    break;
                default:
                    redirect('web/seccion/'.$vista, 'refresh');
                    $this->load_data_view('web/seccion'); 
                break;
                
            }
        }
        
    }

    