<?php
class Site extends CI_Controller {
 function __construct()
 {
 parent::__construct();

 }
 function admin()
 {
 $data['_view'] = 'admin/index';
 $this->load->view('layouts/main',$data);
 }

 function web()
 {
 $data['_view'] = 'web/index';
 $this->load->view('layouts/main',$data);
 }
}