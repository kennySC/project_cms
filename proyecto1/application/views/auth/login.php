<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Build-A-Page CMS!</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="<?php echo site_url('resources/img/favicon.png'); ?>" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/login.css'); ?>">
</head>

<body>
    <?php
    if (isset($logout_message)) {
        echo "<div class='login_msg_box success'>" . $logout_message . "</div>";
    }

    if (isset($message_display)) {
        echo "<div class='login_msg_box success' >" . $message_display . "</div>";
    }

    if (isset($error_message)) {
        echo "<div class='login_msg_box warning'>" . $error_message . "</div>";
    }

    if(validation_errors() !== ""){
        echo "<div class='login_msg_box warning'>" . validation_errors() . "</div>";
    }

    ?>
    <div id="main">
        <img src="<?php echo site_url('/resources/img/titulo.png'); ?>" id="img_titulo"><br>
        <label id="lbl_titulo">BUILD-A-PAGE CMS!</label>
        <div id="contenedor_login"><br>
            <div id="login">
                <div id="form_container">
                    <?php echo form_open('auth/login');	?>
                        <label id="lbl_login">Log in</label><br>
                        <input type="text" name="txt_username" id="txt_username" placeholder="USUARIO" title="Usuario" class="cajatexto" /><br />
                        <input type="password" name="txt_password" id="txt_password" placeholder="**********" title="Contraseña" class="cajatexto" /><br />
                        <input type="submit" value="INGRESAR" name="btn_login" id="btn_login" /><br />
                    <?php echo form_close(); ?>                    
                </div>
            </div><br>
        </div>
    </div>
</body>

</html>