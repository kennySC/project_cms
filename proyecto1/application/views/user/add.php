<link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/users.css'); ?>">
<div id="panel_agregar">
    <div class="cabecera_agregar">
        <label id="lbl_agregar_usuario">Agregar Usuario</label>
        <?php echo form_open('admin/index');?>
            <button type="submit" name="btn_volver" id="btn_volver" class="boton" title="Volver"></button>
        <?php echo form_close();?>
    </div>
    <br>
    <div id="divInformacion">
        <br>
        <?php echo form_open('user/agregar'); ?>
            <label id="lbl_username" class="lbl_form_info"><span class="text-danger">* </span>Nombre de Usuario:</label>
            <div class="form_info">
                <input type="text" class="form_txt" name="txt_username" id="txt_username">
                <span class="peligro-texto"><?php echo form_error('txt_username');?></span>
            </div>
            
            <label id="lbl_password" class="lbl_form_info"><span class="text-danger">* </span>Password:</label>
            <div class="form_info">
                <input type="password" class="form_txt" name="txt_password" id="txt_password">
            </div>
            
            <label id="lbl_correo" class="lbl_form_info"><span class="text-danger">* </span>Correo:</label>
            <div class="form_info">
                <input type="email" class="form_txt" name="txt_correo" id="txt_correo">
            </div>

            <label id="lbl_nombrereal" class="lbl_form_info"><span class="text-danger">* </span>Nombre Real:</label>
            <div class="form_info">
                <input type="text" class="form_txt" name="txt_nombrereal" id="txt_nombrereal">
            </div>
            
            <div class="footer_agregar">
	    	    <button type="submit" id="btn_guardar" class="boton" title="Guardar"></button>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>