<link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/users.css'); ?>">
<?php

    if($this->session->flashdata('error')){ echo "<div class='msg_box_user error' >" .  $this->session->flashdata('error') . "</div>"; } 
    if($this->session->flashdata('success')){ echo "<div class='msg_box_user success' >" .  $this->session->flashdata('success') . "</div>"; } 

?>
<div id="panel_agregar">
    <div class="cabecera_agregar">
        <label id="lbl_agregar_usuario">Editar Usuario</label>
        <?php echo form_open('admin/index');?>
            <button type="submit" name="btn_volver" id="btn_volver" class="boton" title="Volver"></button>
        <?php echo form_close();?>
    </div>
    <br>
    <div id="divInformacion">
        <script>
            function abrirInputFile(){
                document.getElementById("txt_file").click();
            }

            function img_onload(){        
                document.getElementById("img_preview").style.visibility = "visible";
                document.getElementById("btn_del_foto").style.visibility = "visible";
            }

            function mostrarImg(val){        
                val.onchange = e => {
                    if(e.target.files[0]){             
                        nom_img = val.value.replace("C:\\fakepath\\", "");
                        extension = nom_img.split('.').pop();
                        var fr = new FileReader();
                        fr.onload = function () {                
                            document.getElementById("img_preview").src = fr.result;   
                            document.getElementById("img_preview").style.visibility = "visible";
                        }
                        fr.readAsDataURL(e.target.files[0]);
                    }            
                }
            }
        </script>
        <br>
        <div id="form_datos">
            <?php echo form_open('user/editar'); ?>
                <label id="lbl_username" class="lbl_form_info"><span class="text-danger">* </span>Nombre de Usuario:</label>
                <div class="form_info">
                    <input type="text" class="form_txt" name="txt_username" id="txt_username" value = "<?php echo $this->session->userdata['logged_in']['username']; ?>">
                    <span class="peligro-texto"><?php echo form_error('txt_username');?></span>
                </div>
                
                <label id="lbl_password" class="lbl_form_info"><span class="text-danger">* </span>Password:</label>
                <div class="form_info">
                    <input type="password" class="form_txt" name="txt_password" id="txt_password">
                </div>
                
                <label id="lbl_correo" class="lbl_form_info"><span class="text-danger" >* </span>Correo:</label>
                <div class="form_info">
                    <input type="email" class="form_txt" name="txt_correo" id="txt_correo" value = "<?php echo $this->session->userdata['logged_in']['email']; ?>">
                </div>

                <label id="lbl_nombrereal" class="lbl_form_info"><span class="text-danger">* </span>Nombre Real:</label>
                <div class="form_info">
                    <input type="text" class="form_txt" name="txt_nombrereal" id="txt_nombrereal" value = "<?php echo $this->session->userdata['logged_in']['nom_real']; ?>">
                </div>
                <br><br>
                <div class="footer_agregar">
                    <button type="submit" id="btn_guardar" class="boton" title="Guardar"></button>
                </div>                
                <div id="actions">
                    <a href="<?php echo site_url('user/eliminar/'.$this->session->userdata['logged_in']['id_usuario']); ?>" id="btn_eliminar" name="btn_eliminar" title="Eliminar">🗙 Eliminar Usuario</a>
                </div>
            <?php echo form_close(); ?>
        </div>
        
        <div id="divFotoUsuario">      
            <?php echo form_open_multipart('user/upload_photo/'.$this->session->userdata['logged_in']['id_usuario']); ?>                              
                <div id="acciones_imgs">
                    <input type="file" id="txt_file" name="txt_file" class="btn btn-info" accept="image/jpeg,image/gif,image/png" style="visibility: hidden;" onclick="mostrarImg(this)"/>
                    <input type="button" id="btn_cargarImagenes" value="" onclick="abrirInputFile()">
                    <button type="submit" id="btn_subirFoto" name="btn_subirFoto" class="">Subir</button>
                </div>
            <?php echo form_close(); ?>            
            <img id="img_preview" src="<?php echo site_url('/resources/photos/'. $this->session->userdata['logged_in']['foto']);?>" id="img_usuario" style="visibility: hidden;" onload="img_onload()">
            <a href="<?php echo site_url('user/eliminar_foto/'.$this->session->userdata['logged_in']['id_usuario']); ?>" id="btn_del_foto" name="btn_del_foto" title="Eliminar Foto" style="visibility: hidden;">Eliminar foto</a>
        </div>
    </div>
</div>