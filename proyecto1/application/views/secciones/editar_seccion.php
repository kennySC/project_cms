<link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/secciones.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/editSecciones.css'); ?>">
<div id="main_panel">
<?php 
    if($this->session->flashdata('error')){ echo "<div class='msg_box_user error' >" .  $this->session->flashdata('error') . "</div>"; } 
    if($this->session->flashdata('success')){ echo "<div class='msg_box_user success' >" .  $this->session->flashdata('success') . "</div>"; } 
    if($this->session->flashdata('warning')){ echo "<div class='msg_box_user warning' >" .  $this->session->flashdata('warning') . "</div>"; } 
    //  Preparamos los datos dependiendo de que tipo de seccion se va a editar
    $id; $titulo; $banner; $detalle; $disable;
    if($tipo == "ini"){
        $id = $seccion['id']; $banner = $seccion['banner']; $detalle = $seccion['detalle'];
        $titulo = (($seccion['tipo'] == "I") ? "Inicio" : "Quines Somos");        
        $disable = "disabled";
        $tabla = "sec_inicio_quienessomos";
    }else{
        $id = $seccion['id_seccion']; $titulo = $seccion['titulo_seccion']; $banner = $seccion['banner_seccion']; $detalle = $seccion['detalle_seccion'];
        $disable = "";
        $tabla = "secciones";
    }
?>
<script>
        function abrirInputFile(){
            document.getElementById("txt_banner").click();
        }        

        function loadIMG(val){
            val.style.visibility = "visible";           
            document.getElementById("btn_del_banner").style.visibility = "visible"; 
        }

        function mostrarImg(val){        
            val.onchange = e => {
                if(e.target.files[0]){             
                    nom_img = val.value.replace("C:\\fakepath\\", "");                                        
                    var fr = new FileReader();
                    fr.onload = function () {                              
                        document.getElementById("img_preview").src = fr.result;   
                        document.getElementById("img_preview").style.visibility = "visible";
                        document.getElementById("btn_subirBanner").style.visibility = "visible";
                        document.getElementById("btn_del_banner").style.visibility = "visible";
                    }
                    fr.readAsDataURL(e.target.files[0]);                  
                }            
            }
        }
    </script>
    <div id="divTituloPantalla">
        <label id="lbl_pantalla">Editando Seccion</label>
    </div><br>
    <div id="banner_actions"><!--Div para el fil chooser donde se va a subir/escoger el banner-->                        
        <?php echo form_open('admin/lista_secciones');?>
            <button type="submit" name="btn_volver" id="btn_volver" class="boton" title="Volver"></button>
        <?php echo form_close();?>        
        <img id="img_preview" src="<?php echo site_url('/resources/files/'.$banner) ?>" alt="" style="visibility: hidden;" onload="loadIMG(this)">        
        
        <?php echo form_open_multipart("seccion/subir_banner/".$tabla."/".$id."/editar_seccion"); ?>
            <input type="button" id="btn_loadBanner_e" value="" onclick="abrirInputFile()">       
            <button type="submit" class="btn_banner_actions" id="btn_subirBanner" name="btn_subirBanner" class="">Subir</button> 
            <input type="file" name="txt_banner" id="txt_banner" title="Subir Imagen" accept="image/jpeg,image/gif,image/png" style="visibility: hidden;" onclick="mostrarImg(this)">
        <?php echo form_close(); ?>

        <a id="btn_del_banner" href="<?php echo site_url('seccion/eliminar_banner/'.$tabla.'/'.$id.'/editar_seccion'); ?>" style="visibility: hidden;">Eliminar Banner</a>  
    </div>
    <?php echo form_open('seccion/editar/'.$tabla.'/'.$id);?>
        <div id="divContenido"> <!--Div donde va a ir el 'contenido' de la seccion (titulo y detalle)-->
            <input class="datos" <?php echo $disable ?> type="text" name="txt_titulo" id="txt_titulo" placeholder="Titulo de la seccion" value="<?php echo $titulo ?>"><br>
            <textarea class="datos" name="txt_detalle" id="txt_detalle" cols="60" rows="15" placeholder="Detalle"><?php echo $detalle ?></textarea>            
        </div>
        <div class="footer_agregar">
            <label id="lbl_guardarcambios">Guarda tus cambios!</label>
            <button type="submit" id="btn_guardar" class="boton" title="Guardar"></button>
        </div>
    <?php echo form_close();?>    
</div>