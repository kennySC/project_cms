<link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/secciones.css'); ?>">
<?php if($this->session->userdata['logged_in']['logged_in']==TRUE){ ?>

<div id="pagina_editar_servicio">
    <div id="divTituloPantalla">
        <label id="lbl_pantalla">Editando Servicio</label>
    </div><br>
    <?php echo form_open('seccion/lista_servicios');?>
        <button type="submit" name="btn_volver" id="btn_volver" class="boton" title="Volver"></button>
    <?php echo form_close();?>
    <div id="contenedor_servicio">
        <div class="block_servicio">
            <?php echo form_open('seccion/editar_servicio/' . $servicio['id_servicio']);?>
                <input class="datos_servicio" type="text" name="txt_nombre_servicio" id="txt_nombre_servicio" placeholder="<?php echo $servicio['nom_servicio'] ?>"><br>
                <textarea class="datos_servicio" name="txt_detalle_servicio" id="txt_detalle_servicio" cols="70" rows="5" placeholder=""><?php echo $servicio['detalle_servicio']?></textarea>
                <button class="datos_servicio" type="submit" id="btn_save" name="btn_save" value="btn_save" title="Editar">EDITAR</button>
            <?php echo form_close();?>
        </div>
    </div>
</div>

<?php 
    }else {
        header("location: " . base_url()); //dirección de arranque especificada en config.php y luego en routes.php
    } 
?>