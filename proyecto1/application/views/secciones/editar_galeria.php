<link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/secciones.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/editGaleria.css'); ?>">
<?php
    if($this->session->flashdata('error')){ echo "<div class='msg_box_user error' >" .  $this->session->flashdata('error') . "</div>"; } 
    if($this->session->flashdata('success')){ echo "<div class='msg_box_user success' >" .  $this->session->flashdata('success') . "</div>"; } 
    if($this->session->flashdata('warning')){ echo "<div class='msg_box_user warning' >" .  $this->session->flashdata('warning') . "</div>"; } 
    if(count($imgs) > 9){
        echo "<div class='msg_box_user warning' >No puede agregar mas imagenes ya que se llego al limite</div>";
    }
?>
<div id="main_container">
    <div id="divTituloPantalla">
        <label id="lbl_pantalla">Editando Galeria</label>
    </div><br>
    <?php echo form_open('admin/index');?>
        <button type="submit" name="btn_volver" id="btn_volver" class="boton" title="Volver"></button>
    <?php echo form_close();?>
    <script>
        function cancelar(){
            document.getElementById("txt_img").value = "";      
            document.getElementById("img_porSubir").src = "";   
            document.getElementById("img_porSubir").style.visibility = "hidden";                      
        }

        function abrirInputFile(num_input){
            if(num_input == 1){
                document.getElementById("txt_banner").click();
            }            
            else{
                document.getElementById("txt_img").click();
            }
        }

        function img_onload(val){        
            val.style.visibility = "visible";
            document.getElementById("btn_del_banner").style.visibility = "visible";
        }

        function mostrarImg(val, num_prev){        
            val.onchange = e => {
                if(e.target.files[0]){             
                    nom_img = val.value.replace("C:\\fakepath\\", "");
                    extension = nom_img.split('.').pop();
                    var fr = new FileReader();
                    fr.onload = function () {      
                        if(num_prev == 1){
                            document.getElementById("img_preview").src = fr.result;   
                            document.getElementById("img_preview").style.visibility = "visible";
                        }else{
                            document.getElementById("img_porSubir").src = fr.result;   
                            document.getElementById("img_porSubir").style.visibility = "visible";
                        }                               
                    }
                    fr.readAsDataURL(e.target.files[0]);
                }            
            }
        }
    </script>
    <div id="banner_actions">
        Banner
        <?php echo form_open_multipart("seccion/subir_banner/sec_galeria/1/editar_galeria"); ?>
            <input type="file" id="txt_banner" name="txt_banner" class="btn btn-info" accept="image/jpeg,image/gif,image/png" style="visibility: hidden;" onclick="mostrarImg(this, 1)"/>
            <input type="button" class="btn_banner_actions" id="btn_cargarImagenes" value="Elije un Banner" onclick="abrirInputFile(1)">
            <button type="submit" class="btn_banner_actions" id="btn_subirBanner" name="btn_subirBanner" class="">Subir</button>
        <?php echo form_close(); ?>
        <img id="img_preview" src="<?php echo site_url('/resources/files/'. $banner_gal['banner']);?>" alt="" title="vista previa banner" onload="img_onload(this)">
        <a id="btn_del_banner" href="<?php echo site_url('seccion/eliminar_banner/sec_galeria/1/editar_galeria'); ?>" style="visibility: hidden;">Eliminar Banner</a>  
    </div>
    <div id="imgs">
        <div id="imgs_actions">
            Añade una Imagen a la Galeria
            <?php echo form_open_multipart("seccion/add_img_galeria"); ?>
                <input type="file" id="txt_img" name="txt_img" class="btn btn-info" accept="image/jpeg,image/gif,image/png" style="visibility: hidden;" onclick="mostrarImg(this, 2)"/>
                <input class="botones_agregar_imagen" type="button" id="btn_selectimg" value="" onclick="abrirInputFile(2)" title="Selecciona Imagen">
                <textarea name="txt_detalle_gal" id="txt_detalle_gal" cols="30" rows="10" maxlength="1000" placeholder="Agregale una descripcion a tu imagen!"></textarea>
                <button class="botones_agregar_imagen" type="submit" id="btn_upload" name="btn_upload" title="Agregar Imagen"></button>
                <input class="botones_agregar_imagen" type="button" id="btn_cancel" value="❌" onclick="cancelar()" title="Descartar Imagen">
            <?php echo form_close(); ?>
            <img id="img_porSubir" src="" alt="" title="vista previa imagen" onload="img_onload(this)">        
        </div>        
        <div id="gallery">
            <?php foreach($imgs as $i){ ?>
                <div class="contenedor_img">
					<a target="_blank" href="<?php echo site_url('resources/files/' . $i['nombre_img']); ?>">

						<img class="galeria_img" src="<?php echo site_url('resources/files/' . $i['nombre_img']); ?>" width="600" height="400">
					</a>
					<div class="descrip_img">
                        <a href="<?php echo site_url('seccion/del_img_galeria/'.$i['id_img']); ?>"> Eliminar imagen </a>
                    </div>
				</div>
            <?php } ?>
        </div>
    </div>
</div>