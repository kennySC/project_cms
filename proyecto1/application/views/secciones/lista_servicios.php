<link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/secciones.css'); ?>">
<?php
    if($this->session->flashdata('error')){ echo "<div class='msg_box_user error' >" .  $this->session->flashdata('error') . "</div>"; } 
    if($this->session->flashdata('success')){ echo "<div class='msg_box_user success' >" .  $this->session->flashdata('success') . "</div>"; } 
    if($this->session->flashdata('warning')){ echo "<div class='msg_box_user warning' >" .  $this->session->flashdata('warning') . "</div>"; } 
?>
<div id="main_panel_servicios">
    <div id="divTituloPantalla">
        <label id="lbl_pantalla">Editando Servicios</label>
    </div><br>
    <?php echo form_open('admin/index');?>
        <button type="submit" name="btn_volver" id="btn_volver" class="boton" title="Volver"></button>
    <?php echo form_close();?>
    <script>
        function abrirInputFile(){
            document.getElementById("txt_banner").click();
        }        

        function loadIMG(val){
            val.style.visibility = "visible";
            document.getElementById("btn_del_banner").style.visibility = "visible";
        }
        
        function mostrarImg(val){        
            val.onchange = e => {
                if(e.target.files[0]){             
                    nom_img = val.value.replace("C:\\fakepath\\", "");                                        
                    var fr = new FileReader();
                    fr.onload = function () {                              
                        document.getElementById("img_preview").src = fr.result;   
                        document.getElementById("img_preview").style.visibility = "visible";                                                  
                    }
                    fr.readAsDataURL(e.target.files[0]);                  
                }            
            }
        }
    </script>
    <div id="div_banner_servicios"><!--Div para el fil chooser donde se va a subir/escoger el banner-->
        <label id="lbl_banner">Elige tu banner!</label><br><br>        
        <input type="button" id="btn_loadBanner" value="" onclick="abrirInputFile()">
        <!--<label id="nombre_img"></label><--En este label podemos cargar el nombre de la imagen que se escoja subir como banner-->
        <img id="img_preview" src="<?php echo site_url('/resources/files/'. $banner['banner']);?>" style="visibility: hidden;" alt="" onload="loadIMG(this)">
        <?php echo form_open_multipart("seccion/subir_banner/sec_servicios/1/lista_servicios"); ?>
            <input type="file" name="txt_banner" id="txt_banner" title="Subir Imagen" accept="image/jpeg,image/gif,image/png" style="visibility: hidden;" onclick="mostrarImg(this)">
            <button type="submit" id="btn_guardar_banner" class="boton" title="Subir Banner"></button>
        <?php echo form_close(); ?>
        <a id="btn_del_banner" href="<?php echo site_url('seccion/eliminar_banner/sec_servicios/1/lista_servicios'); ?>" style="visibility: hidden;">Eliminar Banner</a> 
    </div>
    <div id="agregar_servicio">
        <div id="form_servicio">
            <?php echo form_open_multipart('seccion/agregar_servicio');?>
                <input class="datos_servicio" type="text" name="txt_nombre_servicio" id="txt_nombre_servicio" placeholder="Nombre del servicio"><br>
                <textarea class="datos_servicio" name="txt_detalle_servicio" id="txt_detalle_servicio" cols="70" rows="5" placeholder="Detalle del servicio"></textarea>
                <button class="datos_servicio" type="submit" id="btn_save" name="btn_save" value="btn_save" title="Agregar">AGREGAR</button>
            <?php echo form_close(); ?>
        </div>
    </div>
    <div id="contenedor_servicios">
        <div id="lista_servicios">
            <?php foreach($servicios as $s){ ?>
                <div class="servicio_box">
                    <div class="block">
                        <div class="contenido_servicio">
                            <h2> <?php echo $s['nom_servicio'] ?></h2>
                            <p class="contenido-oculto" id = "cont_serv_<?php echo $s['id_servicio'] ?>"><?php echo $s['detalle_servicio'] ?></p>
                        </div>
                        <a class = "btn_mostrar" id = "btn_mostrar_<?php echo $s['id_servicio'] ?>" onclick= "toggleInfo('cont_serv_<?php echo $s['id_servicio'] ?>','btn_mostrar_<?php echo $s['id_servicio'] ?>')">Mostrar más</a>
                        <!--Opciones para cada div ya sea de editarlo o eliminarlo-->
                        <div id="opciones_servicio">
                            <a href="<?php echo site_url('seccion/editar_servicio/' . $s['id_servicio']); ?>" id="btn_editar" name="btn_editar" title="Editar Servicio">✎</a>
                            <a href="<?php echo site_url('seccion/eliminar_servicio/' . $s['id_servicio']); ?>" id="btn_eliminar" name="btn_eliminar" title="Eliminar Servicio">🗙</a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>