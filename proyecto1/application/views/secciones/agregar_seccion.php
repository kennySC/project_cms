<link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/secciones.css'); ?>">
<div id="main_panel">
    <script>
        function abrirInputFile(){
            document.getElementById("input_banner").click();
        }        

        function loadIMG(val){
            val.style.visibility = "visible";
        }

        function mostrarImg(val){        
            val.onchange = e => {
                if(e.target.files[0]){             
                    nom_img = val.value.replace("C:\\fakepath\\", "");                                        
                    var fr = new FileReader();
                    fr.onload = function () {                              
                        document.getElementById("img_preview").src = fr.result;   
                        document.getElementById("img_preview").style.visibility = "visible";                                                  
                    }
                    fr.readAsDataURL(e.target.files[0]);                  
                }            
            }
        }
    </script>
    <div id="divTituloPantalla">
        <label id="lbl_pantalla">Agregando Seccion</label>
    </div><br>
    <div id="divBanner"><!--Div para el fil chooser donde se va a subir/escoger el banner-->
        <label id="lbl_banner">Elige tu banner!</label><br><br>        
        <input type="button" id="btn_loadBanner" value="" onclick="abrirInputFile()">
        <!--<label id="nombre_img"></label><--En este label podemos cargar el nombre de la imagen que se escoja subir como banner-->
        <img id="img_preview" src="" style="visibility: hidden;" alt="">
        <?php echo form_open('admin/index');?>
            <button type="submit" name="btn_volver" id="btn_volver" class="boton" title="Volver"></button>
        <?php echo form_close();?>
    </div>
    <?php echo form_open_multipart('seccion/agregar');?>
        <div id="divContenido"> <!--Div donde va a ir el 'contenido' de la seccion (titulo y detalle)-->
            <input type="file" name="input_banner" id="input_banner" title="Subir Imagen" accept="image/jpeg,image/gif,image/png" style="visibility: hidden;" onclick="mostrarImg(this)">
            <input class="datos" type="text" name="txt_titulo" id="txt_titulo" placeholder="Titulo de la seccion"><br>
            <textarea class="datos" name="txt_detalle" id="txt_detalle" cols="60" rows="15" placeholder="Detalle"></textarea>
        </div>
        <div class="footer_agregar">
            <label id="lbl_guardarcambios">Guarda tu nueva seccion!</label>
            <button type="submit" id="btn_guardar" class="boton" title="Guardar"></button>
        </div>
    <?php echo form_close();?>
</div>