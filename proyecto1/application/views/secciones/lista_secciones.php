<link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/secciones.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/list_secciones.css'); ?>">
<div id="main_panel">
    <div id="divTituloPantalla">
        <label id="lbl_pantalla">Selecciona una Seccion</label>
    </div><br>
    <?php echo form_open('admin/index'); ?>
    <button type="submit" name="btn_volver" id="btn_volver" class="boton" title="Volver"></button>
    <?php echo form_close(); ?>
    <div id="contenedor">
        <?php
            foreach($ini_quienes as $s){
                echo "<div class='div_seccion'>
                        <span>". (($s['tipo'] == "I") ? "Inicio" : "Quienes Somos") ."</span>
                        <br>
                        <a href='". site_url('seccion/ir_editar/sec_inicio_quienessomos/'.$s["id"]) ."' class='a_editar' >Editar</a>
                      </div>";
            }
            foreach($agreagadas as $s){
                echo "<div class='div_seccion'>
                        <span>". $s['titulo_seccion'] ."</span>
                        <br>
                        <a href='". site_url('seccion/ir_editar/secciones/'.$s["id_seccion"]) ."' class='a_editar' >Editar</a>
                        <a href='". site_url('seccion/eliminar/'.$s["id_seccion"]) ."' class='a_eliminar' >Eliminar</a>
                      </div>";
            }
        ?>
    </div>
</div>