<?php if ($this->session->userdata['logged_in']['logged_in'] == TRUE) { ?>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Build-A-Page CMS!</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="icon" href="<?php echo site_url('resources/img/favicon.png'); ?>" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('resources/css/edicion.css'); ?>">
    </head>

    <body>
        <div class="main_page">
            <!--Div donde tenemos que traer el nombre de usuario logeado y mostrarlo en pantalla-->
            <div id="titulo">
                <label id="lbl_titulo" id="lbl_titulo">Hola <?php echo $this->session->userdata['logged_in']['username'];?>!😊</label>
            </div>
            <div id="exit">
                <?php echo form_open('auth/logout'); ?>
                    <button id="btn_salir" title="Cerrar sesion"></button>
                <?php echo form_close(); ?>
            </div>
            <br>
            <label id="lbl_panel_control">Panel de Control</label>
            <div id="panel_control">
                <div id="opciones_editar">
                    <!--Menu para elegir que editar/agregar, o si salir-->
                    <?php echo form_open('admin/process'); ?>
                        <button type="submit" id="btn_agregar_seccion" name="btn_agregar_seccion" value="btn_agregar_seccion" class="btn_opciones">AGREGAR SECCION</button>
                        <button type="submit" id="btn_editar_seccion" name="btn_editar_seccion" value="btn_editar_seccion" class="btn_opciones">EDITAR SECCION</button>
                    <?php echo form_close(); ?>

                    <?php echo form_open('user/process'); ?>
                        <button type="submit" id="btn_agregar_usuario" name="btn_agregar_usuario" value="btn_agregar_usuario" class="btn_opciones">AGREGAR USUARIO</button>
                        <button type="submit" id="btn_editar_usuario" name="btn_editar_usuario" value="btn_editar_usuario" class="btn_opciones">EDITAR USUARIO</button>
                    <?php echo form_close(); ?>
                </div>
                <div id="opciones_galeria_servicios">
                    <?php echo form_open('seccion/lista_servicios');?>
                        <button type="submit" id="btn_editar_servicios" name="btn_editar_servicios" value="btn_editar_servicios" class="btn_opciones">EDITAR SERVICIOS</button>
                    <?php echo form_close(); ?>
                    <?php echo form_open('seccion/editar_galeria');?>
                        <button type="submit" id="btn_editar_galeria" name="btn_editar_galeria" value="btn_editar_galeria" class="btn_opciones">EDITAR GALERIA</button>
                    <?php echo form_close(); ?>
                    </div>
            </div>
            <div id="footer">
                <label id="lbl_footer">BUILD-A-PAGE CMS!</label>
            </div>
        </div>
    </body>

    </html>
<?php
} else {
    header("location: " . base_url()); //dirección de arranque especificada en config.php y luego en routes.php
}
?>