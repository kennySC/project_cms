<!DOCTYPE html>
	<head>
	<?php if(isset($_COOKIE['tema'])){?>
			<link rel="stylesheet" href="<?php echo site_url('resources/css/styleWebDark.css');?>">
		<?php }else{ ?>
			<link rel="stylesheet" href="<?php echo site_url('resources/css/styleWeb.css');?>">
		<?php } ?>
	</head>
	<body>
		<div id = 'banner_box' >
			<img id = "banner" src="<?php echo site_url('resources/files/'.$banner['banner']);?>" onLoad="cargarBanner()" >
		</div>
		<div id = 'menu_box'>
			<ul>
				<li><a href="<?php echo site_url('web/index'); ?>">Inicio</a></li>
				<li><a href="<?php echo site_url('web/quienesSomos'); ?>">Quienes somos?</a></li>
				<li><a href="<?php echo site_url('web/servicios'); ?>">Servicios</a></li>
				<li><a href="<?php echo site_url('web/galeria'); ?>">Galeria</a></li>
				<li><a href="<?php echo site_url('web/contacto'); ?>">Contacto</a></li>
				<?php foreach($menu as $m){ ?>
					<li><a href="<?php echo site_url('web/seccion/'.$m['id_seccion']); ?>"><?php echo $m['titulo_seccion']?></a></li>
				<?php } ?>
				<li><a href="<?php echo site_url('web/TemaOscuro/servicio'); ?>">
					<?php if(isset($_COOKIE['tema'])){ echo 'Tema Claro'; 
					}else{ 
					echo 'Tema Oscuro';} ?>
				</a></li>
			</ul>
		</div>
		<div id = 'Seccion_box'>
			<label id = 'titulo'>Servicios</label>
			<br>
			<?php foreach($servicios as $s){ ?>
			<div class="servicio_box">
				<div class="block">
					<div class="contenido_servicio">
					<h2> <?php echo $s['nom_servicio'] ?></h2>
					<p class="contenido-oculto" id = "cont_serv_<?php echo $s['id_servicio'] ?>"><?php echo $s['detalle_servicio'] ?></p>
					</div>
					<a class = "btn_mostrar" id = "btn_mostrar_<?php echo $s['id_servicio'] ?>" onclick= "toggleInfo('cont_serv_<?php echo $s['id_servicio'] ?>','btn_mostrar_<?php echo $s['id_servicio'] ?>')">Mostrar más</a>
				</div>
			</div>
			<?php } ?>
		</div>
	</body>
</html>
