<!DOCTYPE html>

<head>
	<?php if (isset($_COOKIE['tema'])) { ?>
		<link rel="stylesheet" href="<?php echo site_url('resources/css/styleWebDark.css'); ?>">
	<?php } else { ?>
		<link rel="stylesheet" href="<?php echo site_url('resources/css/styleWeb.css'); ?>">
	<?php } ?>
</head>

<body>
	<div id='banner_box'>
		<img id="banner" src="<?php echo site_url('resources/files/' . $banner['banner']); ?>" onLoad="cargarBanner()">
	</div>
	<div id='menu_box'>
		<ul>
			<li><a href="<?php echo site_url('web/index'); ?>">Inicio</a></li>
			<li><a href="<?php echo site_url('web/quienesSomos'); ?>">¿Quienes somos?</a></li>
			<li><a href="<?php echo site_url('web/servicios'); ?>">Servicios</a></li>
			<li><a href="<?php echo site_url('web/galeria'); ?>">Galeria</a></li>
			<li><a href="<?php echo site_url('web/contacto'); ?>">Contacto</a></li>
			<?php foreach ($menu as $m) { ?>
				<li><a href="<?php echo site_url('web/seccion/' . $m['id_seccion']); ?>"><?php echo $m['titulo_seccion'] ?></a></li>
			<?php } ?>

			<li><a href="<?php echo site_url('web/TemaOscuro/galeria'); ?>">
					<?php if (isset($_COOKIE['tema'])) {
						echo 'Tema Claro';
					} else {
						echo 'Tema Oscuro';
					} ?>
				</a></li>
		</ul>
	</div>
	<div id='Seccion_box'>

		<label id='titulo'>Galeria</label>
		<br>
		<div id="galeria_box">
			<?php foreach ($imgs as $i) { ?>
				<div class="galeria">
					<a target="_blank" href="<?php echo site_url('resources/files/' . $i['nombre_img']); ?>">

						<img class="galeria_img" src="<?php echo site_url('resources/files/' . $i['nombre_img']); ?>" width="600" height="400">
					</a>
					<div class="descrip_img"><?php echo $i['descripcion_img'] ?></div>
				</div>
			<?php } ?>
		</div>
	</div>
</body>

</html>