<!DOCTYPE html>
	<head>
	<?php if(isset($_COOKIE['tema'])){?>
			<link rel="stylesheet" href="<?php echo site_url('resources/css/styleWebDark.css');?>">
		<?php }else{ ?>
			<link rel="stylesheet" href="<?php echo site_url('resources/css/styleWeb.css');?>">
		<?php } ?>
	</head>
	<body>
		<div id = 'banner_box' >
			<img id = "banner" src="<?php echo site_url('resources/files/'.$info['banner_seccion']);?>" onLoad="cargarBanner()"; >
		</div>
		<div id = 'menu_box'>
			<ul>
				<li><a href="<?php echo site_url('web/index'); ?>">Inicio</a></li>
				<li><a href="<?php echo site_url('web/quienesSomos'); ?>">Quienes somos?</a></li>
				<li><a href="<?php echo site_url('web/servicios'); ?>">Servicios</a></li>
				<li><a href="<?php echo site_url('web/galeria'); ?>">Galeria</a></li>
				<li><a href="<?php echo site_url('web/contacto'); ?>">Contacto</a></li>
				<?php foreach($menu as $m){ ?>
					<li><a href="<?php echo site_url('web/seccion/'.$m['id_seccion']); ?>"><?php echo $m['titulo_seccion']?></a></li>
				<?php } ?>
				<li><a href="<?php echo site_url('web/TemaOscuro/'.$info['id_seccion']); ?>">
					<?php if(isset($_COOKIE['tema'])){ echo 'Tema Claro'; 
					}else{ 
					echo 'Tema Oscuro';} ?>
				</a></li>
			</ul>
		</div>
		<div id = 'Seccion_box'>
			<label id = 'titulo'><?php echo $info['titulo_seccion']?></label>
			<br><br>
			<label id = 'contenido'><?php echo $info['detalle_seccion']?> </label>
		</div>
	</body>
</html>
