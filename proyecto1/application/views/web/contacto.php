<!DOCTYPE html>
	<head>
	<?php if(isset($_COOKIE['tema'])){?>
			<link rel="stylesheet" href="<?php echo site_url('resources/css/styleWebDark.css');?>">
		<?php }else{ ?>
			<link rel="stylesheet" href="<?php echo site_url('resources/css/styleWeb.css');?>">
		<?php } ?>
	</head>
	<body>
		<div id = 'banner_box' style="background: radial-gradient(circle, rgba(116,190,180,1) 0%, rgba(2,0,36,1) 100%);">
			<img id = "banner" src="<?php echo site_url('resources/files/'.$banner['banner']);?>" onLoad="cargarBanner()">
		</div>
		<div id = 'menu_box'>
			<ul>
				<li><a href="<?php echo site_url('web/index'); ?>">Inicio</a></li>
				<li><a href="<?php echo site_url('web/quienesSomos'); ?>">Quienes somos?</a></li>
				<li><a href="<?php echo site_url('web/servicios'); ?>">Servicios</a></li>
				<li><a href="<?php echo site_url('web/galeria'); ?>">Galeria</a></li>
				<li><a href="<?php echo site_url('web/contacto'); ?>">Contacto</a></li>
				<?php foreach($menu as $m){ ?>
					<li><a href="<?php echo site_url('web/seccion/'.$m['id_seccion']); ?>"><?php echo $m['titulo_seccion']?></a></li>
				<?php } ?>
				<li><a href="<?php echo site_url('web/TemaOscuro/contacto'); ?>">
					<?php if(isset($_COOKIE['tema'])){ echo 'Tema Claro'; 
					}else{ 
					echo 'Tema Oscuro';} ?>
				</a></li>
			</ul>
		</div>
		<div id = 'Seccion_box'>
			<label id = 'titulo'>Contacto</label>
			<br><br>
			<?php echo form_open('web/addCom');?>
			<input type="text"  id="txt_nombre"  name="txt_nombre" placeholder="Nombre" pattern = "[A-Za-z\s]{2,64}" required>
			<br>
			<input type="email"  id="txt_email"  name="txt_email" placeholder="Email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2, 4}$" required>
			<br>
			<input type="text"  id="txt_asunto" name="txt_asunto" placeholder="Asunto" required>
			<br>
			<textarea  id="txt_comentario" name="txt_comentario" placeholder="Comentario" required></textarea>
			<br>
			<input type="submit" id ="btn_enviar"value="Enviar">
			<?php echo form_close();?>
		</div>
	</body>
</html>
